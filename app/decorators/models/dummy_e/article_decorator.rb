DummyE::Article.class_eval do
  def summary
    "#{title} - #{text.squeeze}"
  end
end
