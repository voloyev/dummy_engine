# This migration comes from dummy_e (originally 20190221194018)
class CreateDummyEArticles < ActiveRecord::Migration[5.2]
  def change
    create_table :dummy_e_articles do |t|
      t.string :title
      t.text :text

      t.timestamps
    end
  end
end
