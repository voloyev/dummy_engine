# This migration comes from dummy_e (originally 20190221200651)
class CreateDummyEComments < ActiveRecord::Migration[5.2]
  def change
    create_table :dummy_e_comments do |t|
      t.integer :article_id
      t.text :text

      t.timestamps
    end
  end
end
