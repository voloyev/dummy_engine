module DummyE
  class Article < ApplicationRecord
    has_many :comments

    def summary
      "#{title}"
    end
  end
end
